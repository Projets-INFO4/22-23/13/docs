# DOC about process_big_DB.sh

## Brief description

This file is a bash script to run on a terminal. It allows to transform collected data (.log.gz) having the CampusIOT format into a CSV table usable for data analysis.

## Necessary dependencies

Package oui  
Package lora-packet  
A set of JavaScript functions written by Didier Donsez.

## Data verification (count entries)

Counts the **number of lines** in files with the name "campusiot_for_poc-...".

Counts the **number of application messages** in files with the name "campusiot_for_poc-...".

Counts the **number of messages from gateway** in files with the name "campusiot_for_poc-...".

Count the **number of messages from applications** in files with the name "campusiot_for_poc-...".

**gunzip** compresses or decompress a file to standard output without changing the original file.
**gunzip** compress a file and **gunzip -d** decompress a file.    
The **-c** option allows to display the text in a compressed file without decompressing it.  
**wc -l** displays the number of lines in the document.  
**grep** motif allows to search in a file in text format the motif passed in parameter.

## LOGFILE var

It's the file(s) being worked on.

## FILTER var

We only want application messages and no gateways.  
Important to know that the separator caracter can be a comma (,) a semi-colon (;) or a slash (/).

**Regex :**

- ^ : the line starts with what will follow
- \d : it's an integer
- \+ : can happen several times

## SAMPLE var

**$SAMPLE** is a file in which put a certain amount of entries (defined by **$step**) in each iteration of the while loop.

**grep -a** Process a binary file as if it were text and **-E** takes a regex pattern for the search.  
**sed -n "${n},${n1}p"** displays the first lines between n and n1.  
**gzip -c** decompresses the file, keeping the uncompressed version

## SAMPLE verification

We count lines of the file in **$SAMPLE** to verify if there is a problem during the creating of the sample.

## Addition of fields

In this section, we add new fields to the JSON. Sometimes we extract JSON column **(log2json.js)** and sometimes we extract data into individual entries **(splitRxinfo.js)**.

The **sed** command is a substitution command that replaces the first occurrence of a pattern with an other pattern in each line of the input file or input stream.
So, the sed command **sed 's/MSG/application/MSG;application/g'** will replace the first occurrence of **MSG/application** with **MSG;application** in each line of the input.

## Output CSV

We create a .csv file for each iteration of the while loop.

**jq -c** by default, jq pretty-prints JSON output. Using this option will result in more compact output by instead putting each JSON object on a single line.  
**jq '. |= [inputs]'** allow us to not create a JSON array.  
**json2csv** convert JSON to CSV or CSV to JSON. json2csv is now abandonned so it's better to use json-2-csv but we experenced it and got some issues with.
