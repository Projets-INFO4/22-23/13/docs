# Proof of Coverage [INFO4 2022-2023]
# Journal de bord

## Mardi 28/03:  
Présents: DD, Aurélien, Lony, Mathis, Elise  
Lieu: présentiel

Travail en séance: 
- Finalisation de la rédaction du rapport
- Réorganisation du Notebook et des commentaires
- Merge de toutes les parties
- Travail sur les anomalies en fonction de leur date
- Modifications des scripts de process pour faciliter l’usage par de nouveaux utilisateurs
- Création du diagramme de gantt grâce le journal de bord


## Lundi 27/03:
Présents: DD, Aurélien, Lony, Mathis, Elise  
Lieu: présentiel  

Travail en séance: 
- Début rédaction rapport
- Mise au point avec Didier Donsez pour la fin du projet
traitement des anomalies: vérifier que les anomalies n’ont pas été corrigée avec le temps
- Merge du code (traitement des anomalies, unused data)
- Fin de la gestion de l’extraction d’un grand nombre de données
- Ajout de la visualisation d’une map kepler.gl uniquement des anomalies
- Discussion autour du traitement de l’erreur de vitesse de la lumière qui est infaisable mais nous avons implémenté la fonction qui   pourrait permettre la détection de cette erreur dans le futur.  

Plan prochaine séance : 
- Continuer la rédaction du rapport
- Faire les petites tâches de la ToDo List
- Continuer traitement anomalies
- Relecture complète du notebook et correction des fautes / Présentation


## Mardi 21/03:
Présents: Aurélien, Lony, Mathis, Elise  
Lieu: présentiel

Travail en séance: 
- Merge le traitement des données inutilisées et travail sur les bugs apparus après le merge
- Gérer le preprocess d’une plus grande quantité de données
- Début du traitement des anomalies

Plan prochaine séance : 
- Finir traitement des anomalies
- Continuer la gestion du preprocess d’une plus grande quantité de données
- Rapport


## Lundi 20/03:
Présents: Aurélien, Lony, Mathis, Elise  
Lieu: présentiel

Travail en séance:  
- Merge du code produit par Mathis, Lony et Aurélien
- Fix du process.sh (rssi&lsnr)
- Ajustement du calcul des anomalies en utilisant l’écart-type et la moyenne des valeurs des ESP des messages envoyés par distance pour des résultats plus cohérents.
- Découverte de bugs lors de l’utilisation des données 2023 dans le notebook
- Fin du traitement des données inutilisées et réhabilitation d’une grande partie d’entre elles

Plan prochaine séance :   
- Traiter les premières anomalies détectées
- Utiliser une deuxième méthode de détection d’erreurs utilisant des valeurs fixes, (trouver une relation ?)
- Continuer sur l’amélioration de l’algo de répartition des messages
- Fix les bugs apparus dans le notebook suite à l’utilisation des données 2023 
- Modifier le process.sh pour avoir toutes les données campusIOT de 2023
(Voir pour créer plusieurs fichiers CSV)
- Merge la branche test_elise avec le reste


## Mardi 14/03:
Présents: Aurélien, Lony, Mathis, Elise  
Lieu: présentiel

Travail en séance: 
- La code review nous a permis de nous mettre au courant de l’avancement de chacun ainsi que d’avoir une vision plus clair du processing
- Traitement des messages sans localisation [avancement majeur]
- Établissement des antennes suspectes à partir d’un écart relatif des valeurs ESP.
- Clarification du notebook de processing
- Amélioration de la visualisation avec Kepler Gl
- Avancement sur l’algo de répartition des messages, création d’un nuage de point pour visualiser si il y aurait des anomalies
- Compilation de la partie bonus de process.sh
- Merge du preprocessing sur le main

Plan prochaine séance : 
- Traiter les premières anomalies détectées
- Utiliser une deuxième méthode de détection d’erreurs utilisant des valeurs fixes, (trouver une relation ?)
- Continuer sur l’amélioration de l’algo de répartition des messages
- Continuer traitement des messages sans localisation
- **Au début de la prochaine séance, merge le code prêt des différents membres**


## Lundi 13/03:
Présents: Aurélien (présent 1h puis absent car entretien de stage)
Elise (distanciel car malade)
Lony, Mathis
Lieu: présentiel

Travail en séance: 
- Traitement des messages sans localisation [en cours]
- Début du travail pour améliorer l’algo de répartition des messages (l’objectif est de prendre en compte les différentes configurations (datarate,txpower))
- Première version de détection “d’anomalies” de donnés, permet de trouver tous les messages qui ont un ESP anormalement éloigné de la valeurs moyenne de leurs ESP en fonction de la distance d’envoie.
- Début de réflexion sur l’amélioration de l’algorithme
- Debug partie Heatmap

Plan prochaine séance : 
- Continuer traitement des messages sans localisation
- Continuer le travail sur l’upgrade de l’algo de répartition
- Faire une code review pour voir l’avancement de chacun


## Mardi 07/03:
Présents: Aurélien, Lony, Mathis, Elise  
Lieu: distanciel

Travail en séance: 
- Rédaction d’une liste de tâches précises à faire pour le processing des données
- Mise en commun des travaux effectués
- Traitement des messages sans localisation [commencé]
- Fin de la rédaction d’une documentation sur process.sh ainsi que mise au propre du fichier pour ce qui est de la partie fonctionnelle
- Debug Heatmap
- Définition d’une méthode pour trouver les valeurs de référence du jeu de donnés en question

Plan prochaine séance : 
- Continuer traitement des messages sans localisation
- Traiter les valeurs de référence pour détecter les premières anomalies et améliorer les données observées.


## Lundi 06/03:
Présents: DD, Aurélien, Lony, Mathis, Elise  
Lieu: présentiel

Travail en séance: 
- Travail sur le processing : analyse de données en les regroupant par cluster en fonction de la distance d’envoie afin de calculer des valeurs de référence qui permettront de détecter des données suspectes à terme. 
- Travail sur le pré-processing : le script fonctionne avec les fonctionnalités essentiels seulement
- Préparation d’un code review avec la totalité de l’équipe puisque la partie pré-processing est opérationnelle pour déterminer une réattribution des tâches pour la partie processing.

Plan prochaine séance : 
- Code review de la partie processing afin de réattribuer les tâches des membres de l’équipe.
- Avoir une première version du processing qui permette d’analyser les données et détecter les potentielles premières erreurs.


## Mardi 28/20 Soutenance
Présents: Aurélien, Lony, Mathis, Elise et pour la soutenance : Didier Donsez et Olivier Richard  
Lieu: présentiel

Travail en séance:
- Soutenance, prise en considération des retours via les notes prises pendant la soutenance
- Avancement sur le process.sh et sur sa documentation
- compréhension de l'utilisation de keras et de comment placer un point fixe dans un nuage de point
- reprise du code du processing et de l'analyse pour l'améliorer

Plan prochaine séance:
- Continuer de debug process.sh ainsi que sa documentation (actuellement comprendre le fonctionnement de jq)
- continuer processing et analyse


## Lundi 27/20:
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Ecriture d’un notebook de processing et d’analyse en reprenant le travail qui avait été réalisé précédemment par les élèves de l’E3
- Préparation du diaporama de la soutenance
- Début de la lecture des ressources données par le prof sur le machine learning
- Avancement sur le process.sh, malheureusement l’avancement est complexe
- Rédaction d’une documentation sur le fichier process.sh

Plan prochaine séance : 
- Continuer de debug process.sh ainsi que sa documentation
- Continuer le travail sur l'écriture du notebook

## Lundi 20/20
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Lecture, tests et début de re-documentation des notebook de pré-processing.
- Préparation de la réunion avec les anciens étudiants développeurs du projet.
- Avancement sur le process.sh, c’est en bonne voie, probablement fini dans une ou deux séances
- Rédaction d’une documentation sur le fichier process.sh

Plan prochaine séance : 
- Continuer de debug process.sh ainsi que sa documentation
- Continuer le travail sur les rapports Jupyter

PS : Certaines tâches ont été faites pendant les vacances, exemple avec la compréhension du format de donnée CampusIOT.

## Lundi 06/02
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Fin rédaction cahier des charges. Réponse des anciens élèves, réunion prévue le lundi 20/02.
Avancement sur les tâches suivantes :
- Analyse du process.sh de Mr.Donsez dans le dossier utils (lors de cette séance nous avons réduit le nombre d’erreurs lors de l’exécution du script mais il en reste encore)
- Essayer de faire marcher process_orange.sh : erreurs à voir 
- Tentative de faire fonctionner prepareData. sh pour pre-process les nouvelles données 2023

Plan prochaine séance : 
- Préparer la réunion avec les anciens élèves
- Avoir la réponse à nos questions sur les erreurs du code
- Anciennes tâches pas faites :
    - Comprendre les différents format de données 
    - Comprendre le travail des anciens (les rapports Jupyter notamment)

## Lundi 30/01
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Réunion avec Didier Donsez. Passage d’un lien gitHub qui contient un script plus robuste et plus performant. 
- Répartition des tâches :
    - Récupération du script pour le traitement des log v2.0 dans le git Wisec… Encore en cours de compréhension d’usage. 
    - Analyse du process.sh de Mr.Donsez dans le dossier utils

Plan prochaine séance : 
- Préparer des questions précises pour la prochaine réunion avec Mr.Donsez
- Attendre la réponse de l’ancien élève
- Anciennes tâches pas faites :
    - Comprendre les différents format de données 
    - Comprendre le travail des anciens (les rapports Jupyter notamment)

## Lundi 23/01
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Réunion avec Didier Donsez. Récupération des jeux de données, ajout au dépôt Git. Installation des outils de travail -> Jupyter Notebook et Kepler
- Recherches sur la transmission radio, le RSSI, le SNR, l’ESP et la zone de fresnel.
- Regarde le code des anciens élèves pour avoir des questions à leur poser prochainement.

Plan prochaine séance : 
- Comprendre le travail des anciens (les rapports Jupyter notamment)
- Comprendre les différents format de données 
- Récupérer le script NodeJS de Donsez

## Lundi 16/01
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Réunion zoom avec Didier Donsez. Prises de notes. Recherches sur LoRa, Hélium, TTN, CampusIoT.

Plan prochaine séance : 
- Regarder ce qu’est la technologie (document de orange)
- Contacter les anciens de l’E3 pour avoir + d’infos.
- Créer un compte Hélium/TTN
- Attendre que Didier Donsez nous donne accès à CampusIoT


## Lundi 09/01
Présents: DD, Aurélien, Lony, Mathis, Elise
Lieu: présentiel

Travail en séance:
- Choix du projet, découverte de l’équipe.
