# Project report - Proof of Coverage LoRaWAN for CampusIoT
![Logo Polytech x INP](https://cdn.discordapp.com/attachments/1077525678783746098/1092032308443365427/image.png)   
Members : 
 - Coste Aurélien
 - Courant Mathis
 - Dupont Elise
 - Riffard Lony

For everything that we learned during this project, and for his dedication as a supervisor, we would like to thank Mr. Didier Donsez.

## Introduction
The Internet of things (IoT) is the extension of the Internet to things and places in the physical world. The object connected to the Internet is uniquely identified by the network in the same way as a computer connected to the Internet. The connected object has the intelligence to generate data (operating parameters, usage data, physical measurements of its environment, etc.), capture data from its environment and automatically transfer it to the network. At the other end, an IT platform centralizes and processes the collected data and manages the value-added services created by the exploitation of the historical measurements collected over time. The areas of application of the Internet of Things cover whole areas of activity (e-health, e-inclusion, connected sports, connected city, factory of the future, precision agriculture, natural and industrial risk management, smart office, smart building, logistics, soft mobility, etc.). The Internet of Things is considered the third evolution of the Web.

The LoRa/LoRaWAN wireless communication technology allows data to be transmitted over a distance of several kilometers between a connected object and a network of gateways (also called a base station). LoRaWAN can be used to build both public IoD networks (Orange, Objenious, Helium, TheThingsNetwork) and private networks (such as CampusIoT, the Grenoble Alpes University network).

LoRaWAN Proof of Coverage allows a LoRaWAN network to know (and therefore prove) the neighborhood of a station by sending a message from an endpoint (ABP activation) representing the station. Analysis of the duplicate messages received allows a "safe" mapping of the stations to confirm the "real" location of the stations.

So there are connected objects that communicate with gateways, sometimes these gateways are geolocated but this is not always the case. When it's not the case, the owners themselves enter the position of their gateways. This explains a telling example of bad geolocation. When the owner moves his gateway and forgets to update its position (assuming that his gateway is not geolocated).

## Necessary knowledge of radio transmissions

Before all, let's briefly introduce what LoRa technology is. It is a radio communication technique that, as the name says, is “Long Range” oriented. It is meant to send long ranged messages with low power use. This technology is used in IoT to send data from a device to a gateway and then be treated by application servers, as you can see in the schema below.

![Figure 1 - LoRa Network](https://cdn.discordapp.com/attachments/1077525678783746098/1092032495177957396/image.png)
Figure 1 - LoRa Network  

We will not go into details, but we need to understand some of the physical measures that are useful to evaluate the quality and reliability of a signal. 
The three values that we will cover first are RSSI, LSNR and ESP.

The **RSSI** (Received Signal Strength Indicator), it helps to determine the strength of the received signal the value is in dBm and is a negative value. The closer to zero the value is, the stronger the signal is. (Usually between -40dBm and -120dBm)

The **LSNR** (LoRa Signal to Noise Ratio), this value indicates the difference between the RSSI and the noise floor. This value is in dBm. A positive value means that the received signal is above the noise floor, and under the noise floor for negative values. In normal communication techniques, being under the noise floor means that we cannot demodulate the signal, but LoRa is able to demodulate signals that are under the noise floor (usually between 10dBm and -20dBm)

The **ESP** (Estimated Signal Power), this value is a formula that includes the SNR and RSSI that allows it to classify signals.  

    ESP = RSSI – 10*LOG(1 + 10(-SNR/10))

LoRa technology allows different configurations to send messages. As it is a long range and low power technique, the downside is that it can send a limited amount of data. This is the datarate and this value is modulable. The strength of the signal to be sent also can be modified, this is the txpower. We will see through our data that all messages have a specific configuration that are made of the txpower and the datarate.

But why is it interesting to study those values?  
The point is to detect which messages, or devices are suspicious. In the way that it is abnormal to observe a very weak signal from two gateways next to each other or that kind of problem. So it is mandatory to have some references and values that represent those observations, that is why we need such values. 

In the data we analyze, there are also two values used to calculate a distance.
DistanceLoS and DistanceHaversine.
For the first one we need to understand LoS (line of sight), it is the distance in a straight line from the emitter to the receiver.
The second value, distance haversine is a value that gives the distance between two points on a sphere taking count of the curvature of this sphere. In more precise words it is the great-circle distance between two points on a sphere.

Coupled with previous values and GPS coordinates we have all we need to study and analyze all messages.


## Solutions
### Languages and libraries
We took over this project, so there was already existing code. There are scripts in Bash with functions in JavaScript to enrich and convert the dataset which is in JSON format. There are also Jupyter notebooks to analyze the data. Although the project already exists with its codes, it was not impossible to change the language or the visualization software. But the choices made by our predecessors are good, the Jupyter notebooks in Python have an advantage over R. The most used libraries for data analysis are pandas & geopanda and KeplerGl, for most of them, they are Python’s libraries.

### Collecting data
The dataset on which we are working was provided by Mr.Donsez. It is more than 1 million 300 lines of data. This represents 500Mb of compressed data. We also had access to the dataset of the previous years. This allowed us to test our notebook with this old data, while we were pre-processing the 2023 data.

### Import and preprocessing data
The initial data is compressed in a log.gz file, has to be converted into a JSON format that can be read by our libraries. To do this, we were able to retrieve a bash script as well as the JavaScript functions used by the bash script to preprocess the data. The first step was to understand the code provided and this was not easy as there were no comments and no documentation. Moreover, the code was not functional. The second step was to debug the script as well as to write a documentation that explains how the script works. To be effective in debugging this type of file, we need to understand the data format we are working with. This is CampusIoT data, here is an example of a message received by the CampusIoT network stations (in JSON format) :  
![Figure 2 - Example of a message CampusIoT received](https://cdn.discordapp.com/attachments/1077525678783746098/1092032640959389738/image.png)
Figure 2 - Example of a message CampusIoT received  

In addition to the bug fix we changed the output of the data in a CSV file. Previously the data was a bit different, with the 2023 dataset we had to make some changes to adapt the output. For example, it should be taken into account that the separating character between messages can be a comma (,), a semicolon (;) or a slash (/). All this was done in the following file : data/2023/process.sh. At the end of this bash script, we obtain a result.csv file in which each line corresponds to a message. Each column characterizes these messages (with, for example, the fields esp, rssi, lsnr ...). Here is what we get:  
![Figure 3 - Example of CSV output](https://cdn.discordapp.com/attachments/1077525678783746098/1092032752683069501/image.png)
Figure 3 - Example of CSV output  

Since the amount of CampusIoT messages is rather large, the CSV files quickly become large, and some errors appear. That is why we created a second script data/2023/process_big_DB.sh to extract the entire CampusIoT data set without the problems mentioned earlier. To do this, the script creates several CSV files, each containing a certain number of messages. These files are stored in the folder data/2023/results. All that is left to do is to import all this into a DataFrame to start analyzing the data.

NB : We let process.sh available so that we could still test the analysis on a small dataset.

WARNING : process.sh and process_big_DB.sh have to be run when in the folder data/2023.

### Data processing and visualization
#### Prepare raw data
First of all we needed to choose all the usable data from the raw data set. To do so, we decided to delete the columns that were unused for our analysis to accelerate the process a bit and avoid any confusions. We also did not keep all the messages sent by a gateway of itself.
After this first filter we have a large set of messages but some of them are incomplete (missing GPS coordinates, missing values…). To be as precise as possible, it is important to have as much data as we can. So we decided to allow a part of our work to the treatment of those incomplete data that would have been unused otherwise.
We separated the data into two complementary dataframes: one with the complete data and the other with the incomplete data.

#### Treat unused data

There are several different cases of incomplete data:
- Data that has no location so no distance haversine and emitter
- Data that has no altitude so no LoS distance
- Data that combines both problems.

Work done on incomplete data was to try to rehabilitate as much as possible by using the dataframe of complete data. The aim was to compare each receiver of the incomplete dataframe with each receiver of the complete dataframe by grouping by receiver to reduce the complexity of the algorithm. For each correspondence between the receiver found, we apply functions that take as argument a dataframe containing the rows of the selected group and the row to complete:

**TreatementEsp :** If the Esp is the same within plus or minus 10 dBm, there is a chance that the emitter of the incomplete line is the same as that of the complete line so we add this line to the dataframe returned by the function .

If this first processing returns a non-null dataframe, the second processing is started:

**TreatementRssi:** We return the dataframe composed of the complete lines having the Rssi equal to more or less 5 dBm than the incomplete line.

If this processing returns a non-zero dataframe, we proceed to the last processing:

**TreatementLsnr :** We return the dataframe composed of complete lines having the Rssi equal to more or less 3 dBm than the incomplete line.

At the end of this processing, if a non-zero dataframe is obtained, the first line is added to the rehabilitated data. In this way we were able to complete the data.
The data for the rest of the treatment are the sum of the complete data and the rehabilitated data.

#### First visualization
After treating our data to be ready for analysis. We kept the idea of showing graphs of messages to have a first glance of what happened in our data set. We consider that the persons that will use or read this notebook are experts in this domain so we did not overextend the explanations on those graphs.  

![Figure 4 - Example of the graph ESP by Haversine distance](https://cdn.discordapp.com/attachments/1077525678783746098/1092032919884800050/image.png)  
Figure 4 - Example of the graph ESP by Haversine distance   
It represents all the messages sent by a certain emitter (colors) in function of the ESP and the distance. We added the same graph for both DistanceLoS and DistanceHaversine.

Also, the previous group made a heatmap to visualize the lossrate of messages for a certain emitter with different configurations.  

![Figure 5 - Example of the lossrate Heatmap](https://cdn.discordapp.com/attachments/1077525678783746098/1092032989384421386/image.png)
Figure 5 - Example of the lossrate Heatmap  
We did not modify this part as it seemed complete.

#### Analysis : Light-speed
One of the problems that we can encounter is that the signal is virtually faster or slower than the light speed. Virtually because to verify that we use the timestamps and distance data of a message. In our case we do not have the two timestamps of emission and reception, so we are not able to calculate and verify this error. However, we added the function in the notebook meant to detect all those errors such as all the messages faster than light or slower than light by a certain percentage are detected. We used names of fields that do not exist to illustrate.

#### Analysis : divide and rule
After a few visualizations we begin to treat data. The idea here was to compare our data to reference values. But the idea of fixed values seemed imprecise and too specific. Indeed using fixed reference values is impossible, RSSI and SNR are values that depend on too many variables such as obstacles, weather, frequencies, noise, ect. Furthermore, it would have been necessary to keep them up to date manually in function of the technologies changes and standard. So we wanted to use our data set to determine reference values in function of the sending distance.

The first idea was a linear share out of the values into groups in function of the sending distance. So we could treat a group of emitters together with coherent values considering the context of sending. But we saw that extreme values were corrupting the groups as they were only a few in the very close range and in the very long range. In that case they would not be considered as abnormal at all. So we decided to share this data using a logarithmic scale. To do so, we used the library numpy. That solves the problem of extreme values and creates coherent groups of distances. Now that we have split out our data set, we can calculate the mean, standard deviation and variance of each value (RSSI, SNR and ESP) in function of the distance. That will be our reference values.

#### Analysis : Comparison
At this stage, we have our dataset with a new field “group” that determines the group of sending distance of the current message. And we have our reference values for each group. The next step was to compare all of our data with these. But how? Our current solution is to report as “suspicious” all the values that are at two standard deviations from the mean. That means that we consider that 95,6% of messages are correct, and the 4,4% that are the farthest from normal values are suspicious or as we named them in the code, outliers.

#### Treatment of Outliers
We obtain a dataframe made up of all the messages considered outlier.
The goal is to make ratios per pair (emitter, receiver) with suspicious messages on the number of messages exchanged. This allows us to know which are the pairs (emitter, receiver) with an anomaly rate greater than or equal to 50%. These pairs will be considered abnormal, and a boolean anomaly column will be added to the dataframe to report the messages exchanged between these gateways.

We also started to look at the distribution of anomalies over time to check whether they were corrected at the end of the period or not. The function is not finished but it displays the number of anomalies per (emitter, receiver) per month. To finish it, it would be necessary to take out the pairs that have been corrected (whose anomalies occur at the beginning of the period). Also, to upgarde this part it may be necessary to use some AI model for logistic regression.

#### Map visualizations
One of the possible errors that we can detect is that the coordinates of the gateway are false. That is why we wanted to allow the expert that is reading or using the notebook to visualize on a map where the gateways are supposed to be in function of the coordinates in the dataset. We used kepler.gl as the previous group but modified some parameters to allow either 2D or 3D visualization.  

![Figure 6 - 3D Map visualization versus 2D Map visualization](https://cdn.discordapp.com/attachments/1077525678783746098/1092033131743293511/image.png)  
Figure 6 - 3D Map visualization versus 2D Map visualization  
Both configurations use a color code such as blue links are messages with a particular good signal and red ones are the opposite. (visible in the map settings in the top left corner)  

![Figure 7 - 3D Map visualization of errors](https://cdn.discordapp.com/attachments/1077525678783746098/1092033236990959636/image.png)  
Figure 7 - 3D Map visualization of errors  
We also added a map visualization of only the suspicious message so that they can be treated by the expert.

## Conclusion
Through this project, we experienced for the first time a real situation of data analysis in a field that we did not know before. Moreover by taking over a project already existing. We learnt all the basics of LoRa radio communication necessary for the treatment of data. Furthermore, we learnt how to treat large amounts of data using python and the pandas libraries. We managed to correct a pre-processing script in bash to convert the log files into CSV files and to add further information to the data already collected. We have also added to the existing notebook the feature to detect anomalies using statistics and visualize them, allowing both users to act on potential anomalies and both creating a base of training data-set for a future AI model to automatically detect anomalies. For the future of this project, the main goal is to create an AI model that can use the training data to learn and be able to automatically report all errors. Also studying if some errors detected are not already solved using for example AI model of logistic regression to study the trend of errors (if some errors happen in the beginning of a year but never happen again after it probably means that it was already solved). Anomaly detection can also be improved by looking for multipath cases. Indeed, waves can propagate by direct path or by rebounds.

## Annexes
![Our gantt diagram](https://cdn.discordapp.com/attachments/1077525678783746098/1092401978384076941/image.png)  
Because GitLab does not have a native integration for PlantUML we put an image of our Gantt diagram.

```plantuml
@startgantt
title Gantt Diagram
printscale weekly zoom 3
Project starts 2023-01-16
[Understanding the context of the project] lasts 3 week
[Research physical phenomena] starts at 2023-01-20 and lasts 16 days
[Research LoRa & CampusIoT] starts at 2023-01-16 and lasts 15 days
[Mid-term presentation] happens at 2023-02-28
[Mid-term preparation] starts at 2023-02-17 and lasts 11 days
[Interview with previous students] happens at 2023-02-22
[Write pre-processing] starts at [Understanding the context of the project]'s end and lasts 3 week
[Debug step] starts at [Write pre-processing]'s end and lasts 7 days
[Add features for pre-processing] starts at [Debug step]'s end and lasts 22 days
[Debug] starts at [Add features for pre-processing]'s end and lasts 7 days
[Writing pre-processing docs] starts at 2023-02-21 and lasts 15 days
[Big code review] happens at 2023-03-06
[Existing notebook analysis] starts at [Understanding the context of the project]'s end and lasts 14 days
[Write and debug a new notebook] starts at [Existing notebook analysis]'s end and lasts 43 days
[Analysis : divide and rule] starts at 2023-02-20 and lasts 15 days
[Analysis : Comparison] starts at [Analysis : divide and rule]'s end and lasts 12 days
[Treat unused data] starts at 2023-03-07 and lasts 21 days
[Treat suspicious values] starts at 2023-03-07 and lasts 15 days
[Report writing] starts at 2023-03-27 and lasts 8 days
[Final presentation] happens at 2023-04-03


[Understanding the context of the project] is colored in lightcyan
[Research physical phenomena] is colored in lightcyan
[Research LoRa & CampusIoT] is colored in lightcyan
[Write pre-processing] is colored in lightblue
[Debug step] is colored in lightgreen
[Add features for pre-processing] is colored in lightblue
[Writing pre-processing docs] is colored in lightblue
[Debug] is colored in lightgreen
[Existing notebook analysis] is colored in sandybrown
[Write and debug a new notebook] is colored in sandybrown
[Analysis : divide and rule] is colored in sandybrown
[Analysis : Comparison] is colored in sandybrown
[Treat unused data] is colored in sandybrown
[Treat suspicious values] is colored in sandybrown
[Mid-term preparation] is colored in indianred
[Report writing] is colored in indianred
[Mid-term presentation] is colored in purple
[Interview with previous students] is colored in purple
[Big code review] is colored in purple
[Final presentation] is colored in purple
'Color school sessions
2023-01-16 is colored in lavender
2023-01-23 is colored in lavender
2023-01-30 is colored in lavender
2023-02-06 is colored in lavender
2023-02-20 is colored in lavender
2023-02-27 is colored in lavender
2023-02-28 is colored in lavender
2023-03-06 is colored in lavender
2023-03-07 is colored in lavender
2023-03-13 is colored in lavender
2023-03-14 is colored in lavender
2023-03-20 is colored in lavender
2023-03-21 is colored in lavender
2023-03-27 is colored in lavender
2023-03-28 is colored in lavender
@endgantt
```

## References
LoRa Alliance. (s. d.). About LoRaWAN.  
From https://lora-alliance.org/about-lorawan/  

The Things Network. (s. d.). The Things Network.  
From https://www.thethingsnetwork.org/  

CampusIoT. (s. d.). CampusIoT.  
From https://campusiot.github.io/  

Pandas Development Team. (2022). pandas - Python Data Analysis Library. pandas.pydata.org.  
From https://pandas.pydata.org/  

GeoPandas Development Team. (s. d.). GeoPandas 0.10.1 documentation.  
From https://geopandas.org/en/stable/  

CDEBYTE. Setting up a LoRa Node | CDEBYTE, Setting up a LoRa Node.  
From https://www.cdebyte.com/news/375  
