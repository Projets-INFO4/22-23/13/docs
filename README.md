Link of our Git where our code is :
https://gricad-gitlab.univ-grenoble-alpes.fr/thingsat/proof-of-coverage-2023


TUTORIAL :

1. In your terminal, go to data/2023 folder

2. Two options :
    - Run process.sh if you want to test on a small dataset
    - Run process_big_DB.sh
        => This will extract the data in packets of 10,000 lines.
        => You can stop whenever you think you have enough messages.
        => You can also wait until the 1,300,000 lines are extracted.

3. In Jupyter, execute notebook/Data Processing [Campus_IOT].ipynb
